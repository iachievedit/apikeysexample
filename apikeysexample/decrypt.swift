//
// decrypt.swift
// apikeysexample
//
// Copyright © 2016-2019 iAchieved.it. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
import Foundation

func bytesFromHex(hex:String) -> [UInt8] {
  var bytes:[UInt8] = [UInt8](repeating:0, count:hex.count / 2)
  var bidx = 0; var i = 0
  for s in hex.unicodeScalars {
    var v:UInt8
    if (s.value >= 97) {
      v = UInt8(s.value - 87) // Convert 'abcdef' characters
    } else {
      v = UInt8(s.value - 48) // Convert '0123456789' characters
    }
    i += 1
    if (i % 2 == 0) {
      i = 0
      bytes[bidx] <<= 4
      bytes[bidx] |= v
      bidx += 1
    } else {
      bytes[bidx] = v
    }
  }
  return bytes
}

func decrypt(message:[UInt8], key:String, iv:[UInt8]) -> String? {
  var context:mbedtls_blowfish_context = mbedtls_blowfish_context()
  let keybits = UInt32(key.count*8)
  
  // Initialize the Blowfish context and set the key
  mbedtls_blowfish_init(&context)
  mbedtls_blowfish_setkey(&context, key, keybits)

  var decryptIV:[UInt8] = iv
  var ivOffset          = 0
  var output:[UInt8]    = [UInt8](repeating:0, count:message.count)

  if mbedtls_blowfish_crypt_cfb64(&context,
                                MBEDTLS_BLOWFISH_DECRYPT,
                                message.count,
                                &ivOffset,
                                &decryptIV,
                                message,
                                &output) == 0 {
    return String(bytes: output, encoding: String.Encoding.utf8)
  } else {
    return nil
  }
}
